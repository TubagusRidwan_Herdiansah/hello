
<?php
	echo $this->Html->script('/member-assets/js/orgchart/jquery.orgchart.min.js',array('inline' => false));
	echo $this->Html->css('/member-assets/css/jquery.orgchart.min.css', null,array('inline' => false));
?>
<style>
	#chart-container {
	  font-family: Arial;
	  height: 420px;
	  border-radius: 5px;
	  overflow: auto;
	  text-align: center;
	}
</style>
<?php
	$network = json_encode($out);
?>
<?php
$scriptBlock = <<<SC
$(document).ready(function() {

    var datascource = {$network};

    $('#chart-container').orgchart({
      'data' : datascource,
      'nodeContent': 'title',
      'pan': true,
      'zoom': true,
    });
});
SC;

$this->Html->scriptBlock($scriptBlock, array('inline' => false));
?>
<!-- Container-fluid starts-->
<div class="container-fluid">
	<div class="page-header">
		<div class="row">
			<div class="col">
				<div class="page-header-left">
					<h3><?php __('Genealogy Generation Level'); ?> <?php echo $this->params['pass'][0];?></h3>
				</div>
			</div>
		</div>
	</div>
</div>
bandung

<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="card-body">
					<div id="chart-container"></div>
				</div>
			</div>
		</div>
	</div>
</div>



